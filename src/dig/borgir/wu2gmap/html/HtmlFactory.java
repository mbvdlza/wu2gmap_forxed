package dig.borgir.wu2gmap.html;

import dig.borgir.wu2gmap.config.Config;
import dig.borgir.wu2gmap.wu.Altar;
import dig.borgir.wu2gmap.wu.CaveEntrance;
import dig.borgir.wu2gmap.wu.House;
import dig.borgir.wu2gmap.wu.Landmark;
import dig.borgir.wu2gmap.wu.Settlement;
import dig.borgir.wu2gmap.wu.StoneOfSoulfall;
import dig.borgir.wu2gmap.wu.Tower;
import dig.borgir.wu2gmap.wu.WuMap;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;

/**
 *
 * @author Diggu Borgir
 */
public class HtmlFactory {

    public static void generateMainHtml(Config config, WuMap wuMap) throws ClassNotFoundException, SQLException, IOException {
        StringBuilder sb = new StringBuilder();

        sb.append("<!doctype html>");
        sb.append("<html>");
        sb.append("<head>");
        sb.append("<meta charset=\"utf-8\">");
        sb.append("<meta http-equiv=\"X-UA-Compatible\" content=\"chrome=1\">");
        sb.append("<title>");
        sb.append(wuMap.getServerName());
        sb.append("</title>");
        sb.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\">");
        sb.append("");
        sb.append("</head>");
        sb.append("<body>");
        sb.append("<h1>");
        sb.append(wuMap.getServerName());
        sb.append("</h1>");
        sb.append("<p>&nbsp;</p>");
        sb.append("<ul>");
        sb.append("<li>");
        sb.append("<a href=\"map/\" title=\"\">surface map</a>");
        sb.append("</li>");
        sb.append("<li>");
        sb.append("<a href=\"underground/\" title=\"\">underground map</a>");
        sb.append("</li>");
        sb.append("</ul>");
        sb.append("</body>");
        sb.append("</html>");

        Path file = Files.createDirectories(Paths.get(config.getOutputRoot()));
        file = Paths.get(file.toString() + File.separator + "index.html");
        file = Files.createFile(file);
        BufferedWriter writer = Files.newBufferedWriter(file, Charset.forName("UTF-8"));
        writer.append(sb);
        writer.flush();
        writer.close();

        System.out.println(file.toString());
    }

    public static void generateSurfacemapHtml(Config config, WuMap wuMap) throws ClassNotFoundException, SQLException, IOException {
        StringBuilder sb = new StringBuilder();

        int zoomBase = getBaseZoom(wuMap.getMapWidth());

        sb.append("<!doctype html>");
        sb.append("<html>");

        sb.append("<head>");
        sb.append("<meta charset=\"utf-8\">");
        sb.append("<meta http-equiv=\"X-UA-Compatible\" content=\"chrome=1\">");
        sb.append("<title>");
        sb.append(wuMap.getServerName());
        sb.append(" - surface map");
        sb.append("</title>");
        sb.append("<link href=\"../static/map.css\" rel=\"stylesheet\">");
        sb.append("<script src=\"https://maps.googleapis.com/maps/api/js?v=3.exp\"></script>");
        sb.append("<script src=\"https://googlemaps.github.io/js-map-label/src/maplabel-compiled.js\"></script>");

        sb.append("\n");
        sb.append("<script>");

        sb.append("var wuTypeOptions = {");
        sb.append("getTileUrl: function(coord, zoom) {");
        sb.append("var bound = Math.pow(2, zoom);");
        sb.append("return 'tiles/' + zoom + '/' + coord.x + '-' + coord.y + '.png';");
        sb.append("},");
        sb.append("tileSize: new google.maps.Size(256, 256),");
        sb.append("maxZoom:");
        sb.append(config.getMaxZoomLevelOfSurfaceMap());
        sb.append(",");
        sb.append("minZoom:");
        sb.append(config.getMinZoomLevelOfSurfaceMap());
        sb.append(",");
        sb.append("radius: 1738000,");
        sb.append("name: '");
        sb.append(wuMap.getServerName());
        sb.append("'");
        sb.append("};");

        sb.append("\n");
        sb.append("var wuMapType = new google.maps.ImageMapType(wuTypeOptions);");

        sb.append("var pointToLatlng = function(map, point, z){");
        sb.append("var scale = Math.pow(2, z);");
        sb.append("var normalizedPoint = new google.maps.Point((point.x + 1) / scale, (point.y + 1) / scale);");
        sb.append("var latlng = map.getProjection().fromPointToLatLng(normalizedPoint);");
        sb.append("return latlng;");
        sb.append("}");

        sb.append("\n");
        sb.append("var map, marker, label;");
        sb.append("var mDeeds = [];");
        sb.append("var lDeeds = [];");
        sb.append("var mTowers = [];");
        sb.append("var mHouses = [];");
        sb.append("var mMines = [];");
        sb.append("var mStones = [];");

        sb.append(" ");
        sb.append("function toggleDeeds() {");
        sb.append("for (var i = 0; i < mDeeds.length; i++) {");
        sb.append("if (mDeeds[i].getMap() === null) {");
        sb.append("mDeeds[i].setMap(map);");
        sb.append("lDeeds[i].setMap(map);");
        sb.append("} else {");
        sb.append("mDeeds[i].setMap(null);");
        sb.append("lDeeds[i].setMap(null);");
        sb.append("}");
        sb.append("}");
        sb.append("}");

        sb.append(" ");
        sb.append("function toggleTowers() {");
        sb.append("for (var i = 0; i < mTowers.length; i++) {");
        sb.append("if (mTowers[i].getMap() === null) {");
        sb.append("mTowers[i].setMap(map);");
        sb.append("} else {");
        sb.append("mTowers[i].setMap(null);");
        sb.append("}");
        sb.append("}");
        sb.append("}");

        sb.append(" ");
        sb.append("function toggleHouses() {");
        sb.append("for (var i = 0; i < mHouses.length; i++) {");
        sb.append("if (mHouses[i].getMap() === null) {");
        sb.append("mHouses[i].setMap(map);");
        sb.append("} else {");
        sb.append("mHouses[i].setMap(null);");
        sb.append("}");
        sb.append("}");
        sb.append("}");

        sb.append(" ");
        sb.append("function toggleMines() {");
        sb.append("for (var i = 0; i < mMines.length; i++) {");
        sb.append("if (mMines[i].getMap() === null) {");
        sb.append("mMines[i].setMap(map);");
        sb.append("} else {");
        sb.append("mMines[i].setMap(null);");
        sb.append("}");
        sb.append("}");
        sb.append("}");

        sb.append(" ");
        sb.append("function toggleStones() {");
        sb.append("for (var i = 0; i < mStones.length; i++) {");
        sb.append("if (mStones[i].getMap() === null) {");
        sb.append("mStones[i].setMap(map);");
        sb.append("} else {");
        sb.append("mStones[i].setMap(null);");
        sb.append("}");
        sb.append("}");
        sb.append("}");

        sb.append(getAltars1());
        sb.append(getLandmarks1());

        sb.append("\n");
        sb.append(" ");
        sb.append("function initialize() {");

        sb.append("var myLatlng = new google.maps.LatLng(0, 0);");

        sb.append("var mapOptions = {");
        sb.append("center: myLatlng,");
        sb.append("zoom:");
        sb.append(config.getDefaultZoomLevelOfSurfaceMap());
        sb.append(",");
        sb.append("streetViewControl: false,");
        sb.append("mapTypeControlOptions: {");
        sb.append("mapTypeIds: ['indy']");
        sb.append("}");
        sb.append("};");

        sb.append("map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);");

        sb.append("map.mapTypes.set('indy', wuMapType);");
        sb.append("map.setMapTypeId('indy');");

        sb.append("\n");
        sb.append("var deeds = [");
        for (Settlement settlement : wuMap.getSettlements()) {
            sb.append("{x:");
            sb.append(settlement.getTokenX());
            sb.append(",y:");
            sb.append(settlement.getTokenY());
            sb.append(",name:\"");
            sb.append(settlement.getName());
            sb.append("\"},");
        }
        sb.append("];");

        sb.append("for (var i = 0, deed; deed = deeds[i]; i++) {");
        sb.append("marker = new google.maps.Marker({");
        sb.append("position: pointToLatlng(map,new google.maps.Point(deed.x,deed.y),");
        sb.append(zoomBase);
        sb.append("),");
        sb.append("optimized: false,");
        sb.append("icon: '../static/mm_20_green.png',");
        sb.append("title: deed.name,");
        sb.append("map: map");
        sb.append("});");
        sb.append("mDeeds.push(marker);");
        sb.append("label = new MapLabel({");
        sb.append("position: pointToLatlng(map,new google.maps.Point(deed.x,deed.y),");
        sb.append(zoomBase);
        sb.append("),");
        sb.append("text: deed.name,");
        sb.append("fontSize: 10,");
        sb.append("align: 'center',");
        sb.append("map: map");
        sb.append("});");
        sb.append("lDeeds.push(label);");
        sb.append("}");

        sb.append("\n");
        sb.append("var towers = [");
        for (Tower tower : wuMap.getTowers()) {
            sb.append("{x:");
            sb.append(tower.getX());
            sb.append(",y:");
            sb.append(tower.getY());
            sb.append(",name:\"");
            sb.append(tower.getName());
            sb.append("\"},");
        }
        sb.append("];");

        sb.append("for (var i = 0, tower; tower = towers[i]; i++) {");
        sb.append("marker = new google.maps.Marker({");
        sb.append("position: pointToLatlng(map,new google.maps.Point(tower.x,tower.y),");
        sb.append(zoomBase);
        sb.append("),");
        sb.append("optimized: false,");
        sb.append("icon: '../static/mm_20_red.png',");
        sb.append("title: tower.name,");
        sb.append("map: null");
        sb.append("});");
        sb.append("mTowers.push(marker);");
        sb.append("}");

        sb.append("\n");
        sb.append("var houses = [");
        for (House house : wuMap.getHouses()) {
            sb.append("{x:");
            sb.append(house.getX());
            sb.append(",y:");
            sb.append(house.getY());
            sb.append(",name:\"");
            sb.append(house.getName());
            sb.append("\"},");
        }
        sb.append("];");

        sb.append("for (var i = 0, house; house = houses[i]; i++) {");
        sb.append("marker = new google.maps.Marker({");
        sb.append("position: pointToLatlng(map,new google.maps.Point(house.x,house.y),");
        sb.append(zoomBase);
        sb.append("),");
        sb.append("optimized: false,");
        sb.append("icon: '../static/mm_20_purple.png',");
        sb.append("title: house.name,");
        sb.append("map: null");
        sb.append("});");
        sb.append("mHouses.push(marker);");
        sb.append("}");

        sb.append("\n");
        sb.append("var caves = [");
        for (CaveEntrance cave : wuMap.getEntrances()) {
            sb.append("{x:");
            sb.append(cave.getX());
            sb.append(",y:");
            sb.append(cave.getY());
            sb.append(",name:\"");
            sb.append(cave.getName());
            sb.append("\"},");
        }
        sb.append("];");

        sb.append("for (var i = 0, cave; cave = caves[i]; i++) {");
        sb.append("marker = new google.maps.Marker({");
        sb.append("position: pointToLatlng(map,new google.maps.Point(cave.x,cave.y),");
        sb.append(zoomBase);
        sb.append("),");
        sb.append("optimized: false,");
        sb.append("icon: '../static/mm_20_black.png',");
        sb.append("title: cave.name,");
        sb.append("map: null");
        sb.append("});");
        sb.append("mMines.push(marker);");
        sb.append("}");

        sb.append("\n");
        sb.append("var stones = [");
        for (StoneOfSoulfall stone : wuMap.getStonesOfSoulfall()) {
            sb.append("{x:");
            sb.append(stone.getX());
            sb.append(",y:");
            sb.append(stone.getY());
            sb.append(",name:\"");
            sb.append(stone.getName());
            sb.append("\"},");
        }
        sb.append("];");

        sb.append("for (var i = 0, stone; stone = stones[i]; i++) {");
        sb.append("marker = new google.maps.Marker({");
        sb.append("position: pointToLatlng(map,new google.maps.Point(stone.x,stone.y),");
        sb.append(zoomBase);
        sb.append("),");
        sb.append("optimized: false,");
        sb.append("icon: '../static/mm_20_orange.png',");
        sb.append("title: stone.name,");
        sb.append("map: null");
        sb.append("});");
        sb.append("mStones.push(marker);");
        sb.append("}");

        sb.append(getAltars2(wuMap));
        sb.append(getLandmarks2(wuMap));

        sb.append("\n");
        sb.append("var div;");
        sb.append("var legend = document.getElementById('legend');");
        sb.append("div = document.createElement('div');");
        sb.append("div.innerHTML = '<input type=\"checkbox\" checked=\"checked\" onclick=\"toggleDeeds()\"><img src=\"../static/mm_20_green.png\"> deeds';");
        sb.append("legend.appendChild(div);");
        sb.append("div = document.createElement('div');");
        sb.append("div.innerHTML = '<input type=\"checkbox\" onclick=\"toggleTowers()\"><img src=\"../static/mm_20_red.png\"> towers';");
        sb.append("legend.appendChild(div);");
        sb.append("div = document.createElement('div');");
        sb.append("div.innerHTML = '<input type=\"checkbox\" onclick=\"toggleHouses()\"><img src=\"../static/mm_20_purple.png\"> houses';");
        sb.append("legend.appendChild(div);");
        sb.append("div = document.createElement('div');");
        sb.append("div.innerHTML = '<input type=\"checkbox\" onclick=\"toggleMines()\"><img src=\"../static/mm_20_black.png\"> mines';");
        sb.append("legend.appendChild(div);");
        sb.append("div = document.createElement('div');");
        sb.append("div.innerHTML = '<input type=\"checkbox\" onclick=\"toggleStones()\"><img src=\"../static/mm_20_orange.png\"> stones of soulfall';");
        sb.append("legend.appendChild(div);");
        sb.append(getAltars3());
        sb.append(getLandmarks3());
        sb.append("map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);");

        // initialize() end
        sb.append("}");

        sb.append("google.maps.event.addDomListener(window, 'load', initialize);");
        sb.append("</script>");
        sb.append("</head>");

        sb.append("\n");
        sb.append("<body>");
        sb.append("<div id=\"map-canvas\"></div>");
        sb.append("<div id=\"legend\">");
        sb.append("<h3>Legend</h3>");
        sb.append("<p>soon...</p>");
        sb.append("<br>");
        sb.append("<br>");
        sb.append("<h3>Options</h3>");
        sb.append("</div>");
        sb.append("</body>");

        sb.append("</html>");

        Path file = Files.createDirectories(Paths.get(config.getOutputRoot() + "map" + File.separator));
        file = Paths.get(file.toString() + File.separator + "index.html");
        file = Files.createFile(file);
        BufferedWriter writer = Files.newBufferedWriter(file, Charset.forName("UTF-8"));
        writer.append(sb);
        writer.flush();
        writer.close();

        System.out.println(file.toString());
    }

    public static void generateUndergroundMapHtml(Config config, WuMap wuMap) throws ClassNotFoundException, SQLException, IOException {
        StringBuilder sb = new StringBuilder();

        int zoomBase = getBaseZoom(wuMap.getMapWidth());

        sb.append("<!doctype html>");
        sb.append("<html>");

        sb.append("<head>");
        sb.append("<meta charset=\"utf-8\">");
        sb.append("<meta http-equiv=\"X-UA-Compatible\" content=\"chrome=1\">");
        sb.append("<title>");
        sb.append(wuMap.getServerName());
        sb.append(" - underground map");
        sb.append("</title>");
        sb.append("<link href=\"../static/map.css\" rel=\"stylesheet\">");
        sb.append("<script src=\"https://maps.googleapis.com/maps/api/js?v=3.exp\"></script>");
        sb.append("<script src=\"https://googlemaps.github.io/js-map-label/src/maplabel-compiled.js\"></script>");

        sb.append("\n");
        sb.append("<script>");

        sb.append("var wuTypeOptions = {");
        sb.append("getTileUrl: function(coord, zoom) {");
        sb.append("var bound = Math.pow(2, zoom);");
        sb.append("return 'tiles/' + zoom + '/' + coord.x + '-' + coord.y + '.png';");
        sb.append("},");
        sb.append("tileSize: new google.maps.Size(256, 256),");
        sb.append("maxZoom:");
        sb.append(config.getMaxZoomLevelOfUndergroundMap());
        sb.append(",");
        sb.append("minZoom:");
        sb.append(config.getMinZoomLevelOfUndergroundMap());
        sb.append(",");
        sb.append("radius: 1738000,");
        sb.append("name: '");
        sb.append(wuMap.getServerName());
        sb.append("'");
        sb.append("};");

        sb.append("\n");
        sb.append("var wuMapType = new google.maps.ImageMapType(wuTypeOptions);");

        sb.append("var pointToLatlng = function(map, point, z){");
        sb.append("var scale = Math.pow(2, z);");
        sb.append("var normalizedPoint = new google.maps.Point((point.x + 1) / scale, (point.y + 1) / scale);");
        sb.append("var latlng = map.getProjection().fromPointToLatLng(normalizedPoint);");
        sb.append("return latlng;");
        sb.append("}");

        sb.append("\n");
        sb.append("var map, marker, label;");
        sb.append("var mDeeds = [];");
        sb.append("var lDeeds = [];");
        sb.append("var mMines = [];");

        sb.append(" ");
        sb.append("function toggleDeeds() {");
        sb.append("for (var i = 0; i < mDeeds.length; i++) {");
        sb.append("if (mDeeds[i].getMap() === null) {");
        sb.append("mDeeds[i].setMap(map);");
        sb.append("lDeeds[i].setMap(map);");
        sb.append("} else {");
        sb.append("mDeeds[i].setMap(null);");
        sb.append("lDeeds[i].setMap(null);");
        sb.append("}");
        sb.append("}");
        sb.append("}");

        sb.append(" ");
        sb.append("function toggleMines() {");
        sb.append("for (var i = 0; i < mMines.length; i++) {");
        sb.append("if (mMines[i].getMap() === null) {");
        sb.append("mMines[i].setMap(map);");
        sb.append("} else {");
        sb.append("mMines[i].setMap(null);");
        sb.append("}");
        sb.append("}");
        sb.append("}");

        sb.append("\n");
        sb.append(" ");
        sb.append("function initialize() {");

        sb.append("var myLatlng = new google.maps.LatLng(0, 0);");

        sb.append("var mapOptions = {");
        sb.append("center: myLatlng,");
        sb.append("zoom:");
        sb.append(config.getDefaultZoomLevelOfUndergroundMap());
        sb.append(",");
        sb.append("streetViewControl: false,");
        sb.append("mapTypeControlOptions: {");
        sb.append("mapTypeIds: ['indy']");
        sb.append("}");
        sb.append("};");

        sb.append("map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);");

        sb.append("map.mapTypes.set('indy', wuMapType);");
        sb.append("map.setMapTypeId('indy');");

        sb.append("\n");
        sb.append("var deeds = [");
        for (Settlement settlement : wuMap.getSettlements()) {
            sb.append("{x:");
            sb.append(settlement.getTokenX());
            sb.append(",y:");
            sb.append(settlement.getTokenY());
            sb.append(",name:\"");
            sb.append(settlement.getName());
            sb.append("\"},");
        }
        sb.append("];");

        sb.append("for (var i = 0, deed; deed = deeds[i]; i++) {");
        sb.append("marker = new google.maps.Marker({");
        sb.append("position: pointToLatlng(map,new google.maps.Point(deed.x,deed.y),");
        sb.append(zoomBase);
        sb.append("),");
        sb.append("optimized: false,");
        sb.append("icon: '../static/mm_20_green.png',");
        sb.append("title: deed.name,");
        sb.append("map: map");
        sb.append("});");
        sb.append("mDeeds.push(marker);");
        sb.append("label = new MapLabel({");
        sb.append("position: pointToLatlng(map,new google.maps.Point(deed.x,deed.y),");
        sb.append(zoomBase);
        sb.append("),");
        sb.append("text: deed.name,");
        sb.append("fontSize: 10,");
        sb.append("align: 'center',");
        sb.append("map: map");
        sb.append("});");
        sb.append("lDeeds.push(label);");
        sb.append("}");

        sb.append("\n");
        sb.append("var caves = [");
        for (CaveEntrance cave : wuMap.getEntrances()) {
            sb.append("{x:");
            sb.append(cave.getX());
            sb.append(",y:");
            sb.append(cave.getY());
            sb.append(",name:\"");
            sb.append(cave.getName());
            sb.append("\"},");
        }
        sb.append("];");

        sb.append("for (var i = 0, cave; cave = caves[i]; i++) {");
        sb.append("marker = new google.maps.Marker({");
        sb.append("position: pointToLatlng(map,new google.maps.Point(cave.x,cave.y),");
        sb.append(zoomBase);
        sb.append("),");
        sb.append("optimized: false,");
        sb.append("icon: '../static/mm_20_black.png',");
        sb.append("title: cave.name,");
        sb.append("map: null");
        sb.append("});");
        sb.append("mMines.push(marker);");
        sb.append("}");

        sb.append("\n");
        sb.append("var div;");
        sb.append("var legend = document.getElementById('legend');");
        sb.append("div = document.createElement('div');");
        sb.append("div.innerHTML = '<input type=\"checkbox\" checked=\"checked\" onclick=\"toggleDeeds()\"><img src=\"../static/mm_20_green.png\"> deeds';");
        sb.append("legend.appendChild(div);");
        sb.append("div = document.createElement('div');");
        sb.append("div.innerHTML = '<input type=\"checkbox\" onclick=\"toggleMines()\"><img src=\"../static/mm_20_black.png\"> mines';");
        sb.append("legend.appendChild(div);");
        sb.append("map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);");

        // initialize() end
        sb.append("}");

        sb.append("google.maps.event.addDomListener(window, 'load', initialize);");
        sb.append("</script>");
        sb.append("</head>");

        sb.append("\n");
        sb.append("<body>");
        sb.append("<div id=\"map-canvas\"></div>");
        sb.append("<div id=\"legend\">");
        sb.append("<h3>Legend</h3>");
        sb.append("<p><span style=\"color:#A9A9A9;\">&#9608;</span> rock wall</p>");
        sb.append("<p><span style=\"color:#DCDCDC;\">&#9608;</span> rock floor</p>");
        sb.append("<p><span style=\"color:#FF0000;\">&#9608;</span> reinforced wall</p>");
        sb.append("<p><span style=\"color:#FFA07A;\">&#9608;</span> reinforced floor</p>");
        sb.append("<p><span style=\"color:#00CED1;\">&#9608;</span> marble</p>");
        sb.append("<p><span style=\"color:#8B4513;\">&#9608;</span> iron</p>");
        sb.append("<p><span style=\"color:#FF1493;\">&#9608;</span> copper</p>");
        sb.append("<p><span style=\"color:#FFD700;\">&#9608;</span> gold</p>");
        sb.append("<p><span style=\"color:#0000FF;\">&#9608;</span> silver</p>");
        sb.append("<p><span style=\"color:#00BFFF;\">&#9608;</span> lead</p>");
        sb.append("<p><span style=\"color:#00FF00;\">&#9608;</span> slate</p>");
        sb.append("<p><span style=\"color:#FFFFFF;\">&#9608;</span> tin</p>");
        sb.append("<p><span style=\"color:#008000;\">&#9608;</span> zinc</p>");
        sb.append("<p><span style=\"color:#FFFFFF;\">&#9608;</span> insanity tile</p>");
        sb.append("<p><span style=\"color:#FFFFFF;\">&#9608;</span> entrance</p>");
        sb.append("<p><span style=\"color:#FFFFFF;\">&#9608;</span> dropshaft</p>");
        sb.append("<p><span style=\"color:#FFFFFF;\">&#9608;</span> water</p>");
        sb.append("<br>");
        sb.append("<h3>Options</h3>");
        sb.append("</div>");
        sb.append("</body>");

        sb.append("</html>");

        Path file = Files.createDirectories(Paths.get(config.getOutputRoot() + "underground" + File.separator));
        file = Paths.get(file.toString() + File.separator + "index.html");
        file = Files.createFile(file);
        BufferedWriter writer = Files.newBufferedWriter(file, Charset.forName("UTF-8"));
        writer.append(sb);
        writer.flush();
        writer.close();

        System.out.println(file.toString());
    }

    /**
     *
     * @param mapSize map size in tiles
     * @return Google Maps zoom level at which 1 tile = 1 pixel (for specified
     * map size)
     */
    private static int getBaseZoom(int mapSize) {
        int base = 0;
        if (mapSize == 256) {
            base = 0;
        }
        if (mapSize == 512) {
            base = 1;
        }
        if (mapSize == 1024) {
            base = 2;
        }
        if (mapSize == 2048) {
            base = 3;
        }
        if (mapSize == 4096) {
            base = 4;
        }
        if (mapSize == 8192) {
            base = 5;
        }
        if (mapSize == 16384) {
            base = 6;
        }
        return base;
    }

    private static StringBuilder getAltars1() {
        StringBuilder sb = new StringBuilder();

        sb.append("var mAltars = [];");
        sb.append("var lAltars = [];");

        sb.append(" ");
        sb.append("function toggleAltars() {");
        sb.append("for (var i = 0; i < mAltars.length; i++) {");
        sb.append("if (mAltars[i].getMap() === null) {");
        sb.append("mAltars[i].setMap(map);");
        sb.append("lAltars[i].setMap(map);");
        sb.append("} else {");
        sb.append("mAltars[i].setMap(null);");
        sb.append("lAltars[i].setMap(null);");
        sb.append("}");
        sb.append("}");
        sb.append("}");

        return sb;
    }

    private static StringBuilder getAltars2(WuMap wuMap) {
        StringBuilder sb = new StringBuilder();

        int zoomBase = getBaseZoom(wuMap.getMapWidth());

        sb.append("\n");
        sb.append("var altars = [");
        for (Altar altar : wuMap.getAltars()) {
            sb.append("{x:");
            sb.append(altar.getX());
            sb.append(",y:");
            sb.append(altar.getY());
            sb.append(",name:\"");
            sb.append(altar.getName());
            sb.append("\"},");
        }
        sb.append("];");

        sb.append("for (var i = 0, altar; altar = altars[i]; i++) {");
        sb.append("marker = new google.maps.Marker({");
        sb.append("position: pointToLatlng(map,new google.maps.Point(altar.x,altar.y),");
        sb.append(zoomBase);
        sb.append("),");
        sb.append("optimized: false,");
        sb.append("icon: '../static/mm_20_gray.png',");
        sb.append("title: altar.name,");
        sb.append("map: null");
        sb.append("});");
        sb.append("mAltars.push(marker);");
        sb.append("label = new MapLabel({");
        sb.append("position: pointToLatlng(map,new google.maps.Point(altar.x,altar.y),");
        sb.append(zoomBase);
        sb.append("),");
        sb.append("text: altar.name,");
        sb.append("fontSize: 10,");
        sb.append("align: 'center',");
        sb.append("map: null");
        sb.append("});");
        sb.append("lAltars.push(label);");
        sb.append("}");

        return sb;
    }

    private static StringBuilder getAltars3() {
        StringBuilder sb = new StringBuilder();
        sb.append("div = document.createElement('div');");
        sb.append("div.innerHTML = '<input type=\"checkbox\" onclick=\"toggleAltars()\"><img src=\"../static/mm_20_gray.png\"> altars';");
        sb.append("legend.appendChild(div);");
        return sb;
    }

    private static StringBuilder getLandmarks1() {
        StringBuilder sb = new StringBuilder();

        sb.append("var mLandmarks = [];");
        sb.append("var lLandmarks = [];");

        sb.append(" ");
        sb.append("function toggleLandmarks() {");
        sb.append("for (var i = 0; i < mLandmarks.length; i++) {");
        sb.append("if (mLandmarks[i].getMap() === null) {");
        sb.append("mLandmarks[i].setMap(map);");
        sb.append("lLandmarks[i].setMap(map);");
        sb.append("} else {");
        sb.append("mLandmarks[i].setMap(null);");
        sb.append("lLandmarks[i].setMap(null);");
        sb.append("}");
        sb.append("}");
        sb.append("}");

        return sb;
    }

    private static StringBuilder getLandmarks2(WuMap wuMap) {
        StringBuilder sb = new StringBuilder();

        int zoomBase = getBaseZoom(wuMap.getMapWidth());

        sb.append("\n");
        sb.append("var landmarks = [");
        for (Landmark landmark : wuMap.getLandmarks()) {
            sb.append("{x:");
            sb.append(landmark.getX());
            sb.append(",y:");
            sb.append(landmark.getY());
            sb.append(",name:\"");
            sb.append(landmark.getName());
            sb.append("\"},");
        }
        sb.append("];");

        sb.append("for (var i = 0, landmark; landmark = landmarks[i]; i++) {");
        sb.append("marker = new google.maps.Marker({");
        sb.append("position: pointToLatlng(map,new google.maps.Point(landmark.x,landmark.y),");
        sb.append(zoomBase);
        sb.append("),");
        sb.append("optimized: false,");
        sb.append("icon: '../static/mm_20_gray.png',");
        sb.append("title: landmark.name,");
        sb.append("map: null");
        sb.append("});");
        sb.append("mLandmarks.push(marker);");
        sb.append("label = new MapLabel({");
        sb.append("position: pointToLatlng(map,new google.maps.Point(landmark.x,landmark.y),");
        sb.append(zoomBase);
        sb.append("),");
        sb.append("text: landmark.name,");
        sb.append("fontSize: 10,");
        sb.append("align: 'center',");
        sb.append("map: null");
        sb.append("});");
        sb.append("lLandmarks.push(label);");
        sb.append("}");

        return sb;
    }

    private static StringBuilder getLandmarks3() {
        StringBuilder sb = new StringBuilder();
        sb.append("div = document.createElement('div');");
        sb.append("div.innerHTML = '<input type=\"checkbox\" onclick=\"toggleLandmarks()\"><img src=\"../static/mm_20_gray.png\"> landmarks';");
        sb.append("legend.appendChild(div);");
        return sb;
    }

}
