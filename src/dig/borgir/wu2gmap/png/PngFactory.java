package dig.borgir.wu2gmap.png;

import ar.com.hjg.pngj.ImageInfo;
import ar.com.hjg.pngj.ImageLineHelper;
import ar.com.hjg.pngj.ImageLineInt;
import ar.com.hjg.pngj.PngWriter;
import com.wurmonline.mesh.Tiles;
import dig.borgir.wu2gmap.config.Config;
import dig.borgir.wu2gmap.wu.Tower;
import dig.borgir.wu2gmap.wu.WuMap;
import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

/**
 *
 * @author Diggu Borgir
 */
public class PngFactory {

    private static Tiles.Tile[][] undergroundTiles;
    private static int maxX;
    private static int maxY;

    public static void generateTiles(Config config, WuMap wuMap) throws FileNotFoundException, SQLException, ClassNotFoundException, IOException, InterruptedException {

        int[] scalesSurface = getNeededScaleLevels(wuMap.getMapWidth(), config.getMaxZoomLevelOfSurfaceMap());
        for (int i = 0; i < scalesSurface.length; i++) {
            generateSurfacePng(config, wuMap, scalesSurface[i]);
        }

        int[] scalesUnderground = getNeededScaleLevels(wuMap.getMapWidth(), config.getMaxZoomLevelOfUndergroundMap());
        for (int i = 0; i < scalesUnderground.length; i++) {
            generateUndergroundPng(config, wuMap, scalesUnderground[i]);
        }
    }

    /**
     * which PNG scale levels are needed for this combination of "map size" and
     * "max zoom level"
     *
     * @param mapSize map size (in tiles)
     * @param maxZoom max allowed zoom level (0, 1, 2, 3...)
     * @return
     */
    private static int[] getNeededScaleLevels(int mapSize, int maxZoom) {
        int[] scales = new int[]{};

        if (mapSize == 16384) {
            scales = new int[]{1};
        }
        if (mapSize == 8192) {
            if (maxZoom == 6) {
                scales = new int[]{1, 2};
            } else {
                scales = new int[]{1};
            }
        }
        if (mapSize == 4096) {
            if (maxZoom == 6) {
                scales = new int[]{1, 2, 4};
            } else if (maxZoom == 5) {
                scales = new int[]{1, 2};
            } else {
                scales = new int[]{1};
            }
        }
        if (mapSize == 2048) {
            if (maxZoom == 6) {
                scales = new int[]{1, 2, 4, 8};
            } else if (maxZoom == 5) {
                scales = new int[]{1, 2, 4};
            } else if (maxZoom == 4) {
                scales = new int[]{1, 2};
            } else {
                scales = new int[]{1};
            }
        }
        if (mapSize == 1024) {
            if (maxZoom == 6) {
                scales = new int[]{1, 2, 4, 8, 16};
            } else if (maxZoom == 5) {
                scales = new int[]{1, 2, 4, 8};
            } else if (maxZoom == 4) {
                scales = new int[]{1, 2, 4};
            } else if (maxZoom == 3) {
                scales = new int[]{1, 2};
            } else {
                scales = new int[]{1};
            }
        }
        if (mapSize == 512) {
            if (maxZoom == 6) {
                scales = new int[]{1, 2, 4, 8, 16, 32};
            } else if (maxZoom == 5) {
                scales = new int[]{1, 2, 4, 8, 16};
            } else if (maxZoom == 4) {
                scales = new int[]{1, 2, 4, 8};
            } else if (maxZoom == 3) {
                scales = new int[]{1, 2, 4};
            } else if (maxZoom == 2) {
                scales = new int[]{1, 2};
            } else {
                scales = new int[]{1};
            }
        }
        if (mapSize == 256) {
            scales = new int[]{1, 2, 4, 8, 16, 32, 64};
        }

        return scales;
    }

    private static void generateSurfacePng(Config config, WuMap wuMap, Integer scale) throws FileNotFoundException, SQLException, ClassNotFoundException, IOException {

        int x1 = 0;
        int y1 = 0;
        int x2 = wuMap.getMapWidth() - 1;
        int y2 = wuMap.getMapHeight() - 1;

        int[][] towers = new int[wuMap.getMapWidth()][wuMap.getMapHeight()];
        for (Tower tower : wuMap.getTowers()) {
            int x = (tower.getX());
            int y = (tower.getY());

            towers[x][y] = 1;
            towers[x - 1][y] = 1;
            towers[x + 1][y] = 1;

            towers[x][y - 1] = 1;
            towers[x - 1][y - 1] = 1;
            towers[x + 1][y - 1] = 1;

            towers[x][y + 1] = 1;
            towers[x - 1][y + 1] = 1;
            towers[x + 1][y + 1] = 1;
        }

        Color[][] tTiles = wuMap.getSurfaceTiles();
        int[][] water = wuMap.getSurfaceWater();

        int[][] houses = wuMap.getBuildTiles();

        String fileName = scale + ".png";
        System.out.println(config.getOutputRoot() + "map" + File.separator + fileName);
        OutputStream outputStream = new FileOutputStream(config.getOutputRoot() + "map" + File.separator + fileName);

        int imageWidth = (x2 - x1 + 1) * scale;
        int imageHeight = (y2 - y1 + 1) * scale;
        ImageInfo imi = new ImageInfo(imageWidth, imageHeight, 8, true);

        PngWriter png = new PngWriter(outputStream, imi);
        png.getMetadata().setDpi(100.0);
        png.getMetadata().setTimeNow(0);

        for (int row = y1; row <= y2; row++) {

            ImageLineInt iline = new ImageLineInt(imi);
            for (int col = x1; col <= x2; col++) {

                Color c;

                if (towers[col][row] == 1) {
                    // #FF0000
                    c = new Color(255, 0, 0, 255);
                } else if (houses[col][row] == 1) {
                    // #800080
                    c = new Color(128, 0, 128, 255);
                } else if (tTiles[col][row] != null) {
                    c = tTiles[col][row];
                } else {
                    c = new Color(255, 255, 255, 255);
                }

                int r = c.getRed();
                int g = c.getGreen();
                int b = c.getBlue();

                if (water[col][row] == 1) {
                    r = (int) (r * 0.2f + 0.4f * 0.4f * 256f);
                    g = (int) (g * 0.2f + 0.5f * 0.4f * 256f);
                    b = (int) (b * 0.2f + 1.0f * 0.4f * 256f);
                }

                for (int i = 0; i < scale; i++) {
                    ImageLineHelper.setPixelRGBA8(iline, scale * (col - x1) + i, r, g, b, c.getAlpha());
                }
            }

            for (int i = 0; i < scale; i++) {
                png.writeRow(iline);
            }
        }
        png.end();

    }

    private static void generateUndergroundPng(Config config, WuMap wuMap, Integer scale) throws FileNotFoundException, SQLException, ClassNotFoundException, IOException {
        maxX = wuMap.getMapWidth() - 1;
        maxY = wuMap.getMapHeight() - 1;

        int x1 = 0;
        int y1 = 0;
        int x2 = maxX;
        int y2 = maxY;

        undergroundTiles = wuMap.getUndergroundTiles();
        int[][] water = wuMap.getUndergroundWater();

        String fileName = scale + ".png";
        System.out.println(config.getOutputRoot() + "underground" + File.separator + fileName);
        OutputStream outputStream = new FileOutputStream(config.getOutputRoot() + "underground" + File.separator + fileName);

        int imageWidth = (x2 - x1 + 1) * scale;
        int imageHeight = (y2 - y1 + 1) * scale;
        ImageInfo imi = new ImageInfo(imageWidth, imageHeight, 8, true);

        PngWriter png = new PngWriter(outputStream, imi);
        png.getMetadata().setDpi(100.0);
        png.getMetadata().setTimeNow(0);

        for (int row = y1; row <= y2; row++) {

            ImageLineInt iline = new ImageLineInt(imi);
            for (int col = x1; col <= x2; col++) {

                Color c;

                if (undergroundTiles[col][row] != null) {
                    Tiles.Tile tile = undergroundTiles[col][row];
                    if (tile == Tiles.Tile.TILE_CAVE) {
                        // pink
                        //c = new Color(255, 192, 203);
                        c = Color.WHITE;
                    } else if (tile == Tiles.Tile.TILE_CAVE_FLOOR_REINFORCED) {
                        // pink 3
                        c = new Color(205, 145, 158);
                    } else if (tile == Tiles.Tile.TILE_CAVE_WALL_REINFORCED) {
                        // pink 4
                        c = new Color(139, 99, 108);
                    } else if (tile == Tiles.Tile.TILE_CAVE_EXIT) {
                        // palevioletred
                        c = new Color(219, 112, 147);
                    } else if (tile == Tiles.Tile.TILE_CAVE_WALL && isCaveWall(col, row)) {
                        c = Color.DARK_GRAY;
                    } else if (tile == Tiles.Tile.TILE_CAVE_WALL_ORE_IRON && isCaveWall(col, row)) {
                        c = Color.RED.darker();
                    } else if (tile == Tiles.Tile.TILE_CAVE_WALL_LAVA && isCaveWall(col, row)) {
                        c = Color.RED;
                    } else if (tile == Tiles.Tile.TILE_CAVE_WALL_ORE_COPPER && isCaveWall(col, row)) {
                        c = Color.GREEN;
                    } else if (tile == Tiles.Tile.TILE_CAVE_WALL_ORE_TIN && isCaveWall(col, row)) {
                        c = Color.GRAY;
                    } else if (tile == Tiles.Tile.TILE_CAVE_WALL_ORE_GOLD && isCaveWall(col, row)) {
                        c = Color.YELLOW.darker();
                    } else if (tile == Tiles.Tile.TILE_CAVE_WALL_ORE_ADAMANTINE && isCaveWall(col, row)) {
                        c = Color.CYAN;
                    } else if (tile == Tiles.Tile.TILE_CAVE_WALL_ORE_GLIMMERSTEEL && isCaveWall(col, row)) {
                        c = Color.YELLOW.brighter();
                    } else if (tile == Tiles.Tile.TILE_CAVE_WALL_ORE_SILVER && isCaveWall(col, row)) {
                        c = Color.LIGHT_GRAY;
                    } else if (tile == Tiles.Tile.TILE_CAVE_WALL_ORE_LEAD && isCaveWall(col, row)) {
                        c = Color.PINK.darker().darker();
                    } else if (tile == Tiles.Tile.TILE_CAVE_WALL_ORE_ZINC && isCaveWall(col, row)) {
                        c = new Color(235, 235, 235);
                    } else if (tile == Tiles.Tile.TILE_CAVE_WALL_SLATE && isCaveWall(col, row)) {
                        c = Color.BLACK;
                    } else if (tile == Tiles.Tile.TILE_CAVE_WALL_MARBLE && isCaveWall(col, row)) {
                        c = Color.WHITE;
                    } else {
                        // unknown
                        c = new Color(215, 215, 215);
                    }
                } else {
                    // unknown
                    c = new Color(215, 215, 215);
                }

                int r = c.getRed();
                int g = c.getGreen();
                int b = c.getBlue();

                if (water[col][row] == 1) {
                    r = (int) (r * 0.2f + 0.4f * 0.4f * 256f);
                    g = (int) (g * 0.2f + 0.5f * 0.4f * 256f);
                    b = (int) (b * 0.2f + 1.0f * 0.4f * 256f);
                }

                for (int i = 0; i < scale; i++) {
                    ImageLineHelper.setPixelRGBA8(iline, scale * (col - x1) + i, r, g, b, c.getAlpha());
                }
            }

            for (int i = 0; i < scale; i++) {
                png.writeRow(iline);
            }

        }
        png.end();

    }

    /**
     *
     * @return is this tile visible directly from inside of a cave?
     */
    private static boolean isCaveWall(int x, int y) {
        boolean b = false;
        Tiles.Tile tile;

        if (x > 0) {
            tile = undergroundTiles[x - 1][y];
            if (tile == Tiles.Tile.TILE_CAVE || tile == Tiles.Tile.TILE_CAVE_EXIT || tile == Tiles.Tile.TILE_CAVE_FLOOR_REINFORCED) {
                b = true;
            }
        }

        if (x < maxX) {
            tile = undergroundTiles[x + 1][y];
            if (tile == Tiles.Tile.TILE_CAVE || tile == Tiles.Tile.TILE_CAVE_EXIT || tile == Tiles.Tile.TILE_CAVE_FLOOR_REINFORCED) {
                b = true;
            }
        }

        if (y > 0) {
            tile = undergroundTiles[x][y - 1];
            if (tile == Tiles.Tile.TILE_CAVE || tile == Tiles.Tile.TILE_CAVE_EXIT || tile == Tiles.Tile.TILE_CAVE_FLOOR_REINFORCED) {
                b = true;
            }
        }

        if (y < maxY) {
            tile = undergroundTiles[x][y + 1];
            if (tile == Tiles.Tile.TILE_CAVE || tile == Tiles.Tile.TILE_CAVE_EXIT || tile == Tiles.Tile.TILE_CAVE_FLOOR_REINFORCED) {
                b = true;
            }
        }

        return b;
    }

}
