package dig.borgir.wu2gmap.config;

import java.io.IOException;

/**
 *
 * @author Diggu Borgir
 */
public class ConfigFactory {
    
    public static Config createConfig() throws IOException {
        return new MyConfig();
    }
    
}
