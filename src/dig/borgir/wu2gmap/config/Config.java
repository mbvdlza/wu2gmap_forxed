package dig.borgir.wu2gmap.config;

/**
 *
 * @author Diggu Borgir
 */
public interface Config {
    
    public String getInputRoot();
    
    public String getOutputRoot();
    
    public String getImageMagickPath();
    
    public String getPathToDbLogin();
    
    public String getPathToDbZones();
    
    public String getPathToDbItems();
    
    public String getPathToSurfaceMesh();
    
    public String getPathToRockMesh();
    
    public String getPathToCaveMesh();
    
    public int getMinZoomLevelOfSurfaceMap();
    
    public int getMaxZoomLevelOfSurfaceMap();
    
    public int getDefaultZoomLevelOfSurfaceMap();
    
    public int getMinZoomLevelOfUndergroundMap();
    
    public int getMaxZoomLevelOfUndergroundMap();
    
    public int getDefaultZoomLevelOfUndergroundMap();
    
}
