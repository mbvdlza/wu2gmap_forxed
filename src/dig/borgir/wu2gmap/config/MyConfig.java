package dig.borgir.wu2gmap.config;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import org.ini4j.Ini;

/**
 *
 * @author Diggu Borgir
 */
class MyConfig implements Config {

    private final String _inputRoot;
    private final String _outputRoot;
    private final String _imPath;
    
    private final int minZoomSurface;
    private final int maxZoomSurface;
    private final int defZoomSurface;
    
    private final int minZoomUnderground;
    private final int maxZoomUnderground;
    private final int defZoomUnderground;
    
    MyConfig() throws IOException {
        System.out.println("Parsing config.ini");

        org.ini4j.Config.getGlobal().setEscape(false);
        Ini ini = new Ini();
        ini.load(new FileReader("config.ini"));

        Ini.Section section = ini.get("paths");
        //TODO check if these are valid directories
        this._inputRoot = this.getIniValue(section, "input dir") + File.separator;
        this._outputRoot = this.getIniValue(section, "output dir") + File.separator;
        this._imPath = this.getIniValue(section, "imagemagick dir") + File.separator;

        section = ini.get("surface map");
        this.minZoomSurface = Integer.parseInt(this.getIniValue(section, "min zoom"));
        this.maxZoomSurface = Integer.parseInt(this.getIniValue(section, "max zoom"));
        this.defZoomSurface = Integer.parseInt(this.getIniValue(section, "default zoom"));
        
        section = ini.get("underground map");
        this.minZoomUnderground = Integer.parseInt(this.getIniValue(section, "min zoom"));
        this.maxZoomUnderground = Integer.parseInt(this.getIniValue(section, "max zoom"));
        this.defZoomUnderground = Integer.parseInt(this.getIniValue(section, "default zoom"));
        
        ini.clear();
    }

    private String getIniValue(Ini.Section section, String key) {
        String s = section.get(key);
        if (s == null) {
            System.out.println("Missing value for: " + key);
            System.exit(0);
        } else {
            s = s.trim();
            if (s.isEmpty()) {
                System.out.println("Missing value for: " + key);
                System.exit(0);
            }
        }
        return s;
    }

    @Override
    public String getInputRoot() {
        return this._inputRoot;
    }

    @Override
    public String getOutputRoot() {
        return this._outputRoot;
    }

    @Override
    public String getPathToDbLogin() {
        return this._inputRoot + "sqlite" + File.separator + "wurmlogin.db";
    }

    @Override
    public String getPathToDbZones() {
        return this._inputRoot + "sqlite" + File.separator + "wurmzones.db";
    }

    @Override
    public String getPathToDbItems() {
        return this._inputRoot + "sqlite" + File.separator + "wurmitems.db";
    }

    @Override
    public String getPathToSurfaceMesh() {
        return this._inputRoot + "top_layer.map";
    }

    @Override
    public String getPathToRockMesh() {
        return this._inputRoot + "rock_layer.map";
    }

    @Override
    public String getPathToCaveMesh() {
        return this._inputRoot + "map_cave.map";
    }

    @Override
    public int getMinZoomLevelOfSurfaceMap() {
        return this.minZoomSurface;
    }

    @Override
    public int getMaxZoomLevelOfSurfaceMap() {
        return this.maxZoomSurface;
    }

    @Override
    public int getDefaultZoomLevelOfSurfaceMap() {
        return this.defZoomSurface;
    }

    @Override
    public int getMinZoomLevelOfUndergroundMap() {
        return this.minZoomUnderground;
    }

    @Override
    public int getMaxZoomLevelOfUndergroundMap() {
        return this.maxZoomUnderground;
    }

    @Override
    public int getDefaultZoomLevelOfUndergroundMap() {
        return this.defZoomUnderground;
    }

    @Override
    public String getImageMagickPath() {
        return this._imPath;
    }

}
