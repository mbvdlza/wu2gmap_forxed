package dig.borgir.wu2gmap.cmd;

import dig.borgir.wu2gmap.config.Config;
import dig.borgir.wu2gmap.wu.WuMap;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;

/**
 *
 * @author Diggu Borgir
 */
public class CmdFactory {

    public static void generateScript(Config config, WuMap wuMap) throws IOException, InterruptedException {

        System.out.println("Generating run2.bat");

        StringBuilder sb = new StringBuilder();

        sb.append("set convertexe=\"");
        sb.append(config.getImageMagickPath());
        sb.append("convert.exe\"");
        sb.append(System.lineSeparator());

        sb.append("set outputdir=");
        sb.append(config.getOutputRoot());
        sb.append(System.lineSeparator());

        sb.append(System.lineSeparator());

        // copy static files
        sb.append("copy static\\*.* \"%outputdir%static\"");
        sb.append(System.lineSeparator());

        sb.append(System.lineSeparator());

        // generate PNG tiles for surface map
        sb.append(getImageMagickScript(config, wuMap.getMapWidth(), "map"));

        sb.append(System.lineSeparator());

        // generate PNG tiles for underground map
        sb.append(getImageMagickScript(config, wuMap.getMapWidth(), "underground"));

        sb.append(System.lineSeparator());

        // delete temporary files
        sb.append("del \"%outputdir%map\\*.png\"");
        sb.append(System.lineSeparator());
        sb.append("del \"%outputdir%underground\\*.png\"");
        sb.append(System.lineSeparator());

        File f = new File("run2.bat");
        f.delete();

        BufferedWriter writer = Files.newBufferedWriter(f.toPath(), Charset.forName("UTF-8"));
        writer.append(sb);
        writer.flush();
        writer.close();
    }

    private static StringBuilder getImageMagickScript(Config config, Integer mapSize, String path) throws IOException, InterruptedException {
        //TODO hardcoded Windows stuff (%%, \)

        StringBuilder sb = new StringBuilder();

        if (isLevelNeeded(config, 0, path)) {
            // tiles for zoom level 0
            sb.append(getMkDir(path, 0));
            if (mapSize == 256) {
                sb.append("copy \"");
                sb.append(getPath(path));
                sb.append("1.png\" \"");
                sb.append(getTilesPath(path, 0));
                sb.append("0-0.png\"");
            } else {
                sb.append("%convertexe% \"");
                sb.append(getPath(path));
                sb.append("1.png\" -resize 256x256 \"");
                sb.append(getTilesPath(path, 0));
                sb.append("0-0.png\"");
            }
            sb.append(System.lineSeparator());
            sb.append(System.lineSeparator());
        }

        if (isLevelNeeded(config, 1, path)) {
            // tiles for zoom level 1
            sb.append(getMkDir(path, 1));
            if (mapSize == 256) {
                sb.append(getCopy(path, "2.png", "map-512.png"));
            } else if (mapSize == 512) {
                sb.append(getCopy(path, "1.png", "map-512.png"));
            } else {
                sb.append(getResize(path, "1.png", 512, "map-512.png"));
            }
            sb.append(getCrop(path, "map-512.png", 1));
            sb.append(getDel(path, "map-512.png"));
            sb.append(System.lineSeparator());
        }

        if (isLevelNeeded(config, 2, path)) {
            // tiles for zoom level 2
            sb.append(getMkDir(path, 2));
            if (mapSize == 256) {
                sb.append(getCopy(path, "4.png", "map-1024.png"));
            } else if (mapSize == 512) {
                sb.append(getCopy(path, "2.png", "map-1024.png"));
            } else if (mapSize == 1024) {
                sb.append(getCopy(path, "1.png", "map-1024.png"));
            } else {
                sb.append(getResize(path, "1.png", 1024, "map-1024.png"));
            }
            sb.append(getCrop(path, "map-1024.png", 2));
            sb.append(getDel(path, "map-1024.png"));
            sb.append(System.lineSeparator());
        }

        if (isLevelNeeded(config, 3, path)) {
            // tiles for zoom level 3
            sb.append(getMkDir(path, 3));
            if (mapSize == 256) {
                sb.append(getCopy(path, "8.png", "map-2048.png"));
            } else if (mapSize == 512) {
                sb.append(getCopy(path, "4.png", "map-2048.png"));
            } else if (mapSize == 1024) {
                sb.append(getCopy(path, "2.png", "map-2048.png"));
            } else if (mapSize == 2048) {
                sb.append(getCopy(path, "1.png", "map-2048.png"));
            } else {
                sb.append(getResize(path, "1.png", 2048, "map-2048.png"));
            }
            sb.append(getCrop(path, "map-2048.png", 3));
            sb.append(getDel(path, "map-2048.png"));
            sb.append(System.lineSeparator());
        }

        if (isLevelNeeded(config, 4, path)) {
            // tiles for zoom level 4
            sb.append(getMkDir(path, 4));
            if (mapSize == 256) {
                sb.append(getCopy(path, "16.png", "map-4096.png"));
            } else if (mapSize == 512) {
                sb.append(getCopy(path, "8.png", "map-4096.png"));
            } else if (mapSize == 1024) {
                sb.append(getCopy(path, "4.png", "map-4096.png"));
            } else if (mapSize == 2048) {
                sb.append(getCopy(path, "2.png", "map-4096.png"));
            } else if (mapSize == 4096) {
                sb.append(getCopy(path, "1.png", "map-4096.png"));
            } else {
                sb.append(getResize(path, "1.png", 4096, "map-4096.png"));
            }
            sb.append(getCrop(path, "map-4096.png", 4));
            sb.append(getDel(path, "map-4096.png"));

            sb.append(System.lineSeparator());
        }

        if (isLevelNeeded(config, 5, path)) {
            // tiles for zoom level 5
            sb.append(getMkDir(path, 5));
            if (mapSize == 256) {
                sb.append(getCopy(path, "32.png", "map-8192.png"));
            } else if (mapSize == 512) {
                sb.append(getCopy(path, "16.png", "map-8192.png"));
            } else if (mapSize == 1024) {
                sb.append(getCopy(path, "8.png", "map-8192.png"));
            } else if (mapSize == 2048) {
                sb.append(getCopy(path, "4.png", "map-8192.png"));
            } else if (mapSize == 4096) {
                sb.append(getCopy(path, "2.png", "map-8192.png"));
            } else if (mapSize == 8192) {
                sb.append(getCopy(path, "1.png", "map-8192.png"));
            } else {
                sb.append(getResize(path, "1.png", 8192, "map-8192.png"));
            }
            sb.append(getCrop(path, "map-8192.png", 5));
            sb.append(getDel(path, "map-8192.png"));
            sb.append(System.lineSeparator());
        }

        if (isLevelNeeded(config, 6, path)) {
            // tiles for zoom level 6
            sb.append(getMkDir(path, 6));
            if (mapSize == 256) {
                sb.append(getCopy(path, "64.png", "map-16384.png"));
            } else if (mapSize == 512) {
                sb.append(getCopy(path, "32.png", "map-16384.png"));
            } else if (mapSize == 1024) {
                sb.append(getCopy(path, "16.png", "map-16384.png"));
            } else if (mapSize == 2048) {
                sb.append(getCopy(path, "8.png", "map-16384.png"));
            } else if (mapSize == 4096) {
                sb.append(getCopy(path, "4.png", "map-16384.png"));
            } else if (mapSize == 8192) {
                sb.append(getCopy(path, "2.png", "map-16384.png"));
            } else {
                sb.append(getCopy(path, "1.png", "map-16384.png"));
            }
            sb.append(getCrop(path, "map-16384.png", 6));
            sb.append(getDel(path, "map-16384.png"));
        }

        return sb;
    }

    private static StringBuilder getPath(String path) {
        StringBuilder sb = new StringBuilder();
        sb.append("%outputdir%");
        sb.append(path);
        sb.append("\\");
        return sb;
    }

    private static StringBuilder getTilesPath(String path, int level) {
        StringBuilder sb = new StringBuilder();
        sb.append("%outputdir%");
        sb.append(path);
        sb.append("\\tiles\\");
        sb.append(level);
        sb.append("\\");
        return sb;
    }

    private static StringBuilder getMkDir(String path, int level) {
        StringBuilder sb = new StringBuilder();
        sb.append("mkdir \"%outputdir%");
        sb.append(path);
        sb.append("\\tiles\\");
        sb.append(level);
        sb.append("\"");
        sb.append(System.lineSeparator());
        return sb;
    }

    private static StringBuilder getCopy(String path, String source, String target) {
        StringBuilder sb = new StringBuilder();
        sb.append("copy \"");
        sb.append(getPath(path));
        sb.append(source);
        sb.append("\" \"");
        sb.append(getPath(path));
        sb.append(target);
        sb.append("\"");
        sb.append(System.lineSeparator());
        return sb;
    }

    private static StringBuilder getDel(String path, String file) {
        StringBuilder sb = new StringBuilder();
        sb.append("del \"");
        sb.append(getPath(path));
        sb.append(file);
        sb.append("\"");
        sb.append(System.lineSeparator());
        return sb;
    }

    private static StringBuilder getResize(String path, String source, int resize, String target) {
        StringBuilder sb = new StringBuilder();
        sb.append("%convertexe% \"");
        sb.append(getPath(path));
        sb.append(source);
        sb.append("\" -resize ");
        sb.append(resize);
        sb.append("x");
        sb.append(resize);
        sb.append(" \"");
        sb.append(getPath(path));
        sb.append(target);
        sb.append("\"");
        sb.append(System.lineSeparator());
        return sb;
    }

    private static StringBuilder getCrop(String path, String file, int level) {
        StringBuilder sb = new StringBuilder();
        sb.append("%convertexe% \"");
        sb.append(getPath(path));
        sb.append(file);
        sb.append("\" -crop 256x256 -set filename:tile \"%%[fx:page.x/256]-%%[fx:page.y/256]\" +repage +adjoin \"");
        sb.append(getTilesPath(path, level));
        sb.append("%%[filename:tile].png\"");
        sb.append(System.lineSeparator());
        return sb;
    }

    private static boolean isLevelNeeded(Config config, int level, String mapType) {
        boolean b = false;
        if ("map".equals(mapType)) {
            // surface map
            if (level >= config.getMinZoomLevelOfSurfaceMap() && level <= config.getMaxZoomLevelOfSurfaceMap()) {
                b = true;
            }
        } else // underground map
         if (level >= config.getMinZoomLevelOfUndergroundMap() && level <= config.getMaxZoomLevelOfUndergroundMap()) {
                b = true;
            }
        return b;
    }

}
