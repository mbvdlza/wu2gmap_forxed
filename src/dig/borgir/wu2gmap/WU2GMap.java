package dig.borgir.wu2gmap;

import dig.borgir.wu2gmap.cmd.CmdFactory;
import dig.borgir.wu2gmap.config.Config;
import dig.borgir.wu2gmap.config.ConfigFactory;
import dig.borgir.wu2gmap.html.HtmlFactory;
import dig.borgir.wu2gmap.png.PngFactory;
import dig.borgir.wu2gmap.wu.WuMap;
import dig.borgir.wu2gmap.wu.WuMapFactory;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Diggu Borgir
 */
public class WU2GMap {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException, InterruptedException {
        System.out.println("START");

        Config config = ConfigFactory.createConfig();

        // let's read all needed data from .map and .db files
        WuMap wuMap = WuMapFactory.createWuMap(config);

        System.out.println("Map size: " + wuMap.getMapWidth() + " tiles");
        if (wuMap.getMapWidth() == 256 || wuMap.getMapWidth() == 1024 || wuMap.getMapWidth() == 2048
                || wuMap.getMapWidth() == 4096 || wuMap.getMapWidth() == 8192 || wuMap.getMapWidth() == 16384) {
            // map size is ok
        } else {
            System.out.println("Unsupported map size.");
            System.exit(0);
        }

        prepareOutputDirectoryStructure(config);

        HtmlFactory.generateMainHtml(config, wuMap);

        HtmlFactory.generateSurfacemapHtml(config, wuMap);

        HtmlFactory.generateUndergroundMapHtml(config, wuMap);

        PngFactory.generateTiles(config, wuMap);

        CmdFactory.generateScript(config, wuMap);

        System.out.println("END");
    }

    private static void prepareOutputDirectoryStructure(Config config) throws IOException {
        System.out.println("Cleaning " + config.getOutputRoot());
        FileUtils.deleteDirectory(new File(config.getOutputRoot() + "map"));
        FileUtils.deleteDirectory(new File(config.getOutputRoot() + "underground"));
        FileUtils.deleteDirectory(new File(config.getOutputRoot() + "static"));
        File file = new File(config.getOutputRoot() + "index.html");
        file.delete();

        System.out.println("Creating " + config.getOutputRoot() + "map" + File.separator);
        Files.createDirectories(Paths.get(config.getOutputRoot() + "map" + File.separator));

        System.out.println("Creating " + config.getOutputRoot() + "map" + File.separator + "tiles" + File.separator);
        Files.createDirectories(Paths.get(config.getOutputRoot() + "map" + File.separator + "tiles" + File.separator));

        System.out.println("Creating " + config.getOutputRoot() + "underground" + File.separator);
        Files.createDirectories(Paths.get(config.getOutputRoot() + "underground" + File.separator));

        System.out.println("Creating " + config.getOutputRoot() + "underground" + File.separator + "tiles" + File.separator);
        Files.createDirectories(Paths.get(config.getOutputRoot() + "underground" + File.separator + "tiles" + File.separator));

        System.out.println("Creating " + config.getOutputRoot() + "static" + File.separator);
        Files.createDirectories(Paths.get(config.getOutputRoot() + "static" + File.separator));
    }

}
