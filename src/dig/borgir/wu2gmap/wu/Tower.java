package dig.borgir.wu2gmap.wu;

/**
 *
 * @author Diggu Borgir
 */
public interface Tower {

    public String getName();

    public Integer getX();

    public Integer getY();

}
