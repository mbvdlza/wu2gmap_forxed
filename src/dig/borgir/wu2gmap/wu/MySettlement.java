package dig.borgir.wu2gmap.wu;

/**
 *
 * @author Diggu Borgir
 */
class MySettlement implements Settlement {

    private final String _name;
    private final Integer _startX;
    private final Integer _endX;
    private final Integer _startY;
    private final Integer _endY;
    private final Integer _tokenX;
    private final Integer _tokenY;
    private final Integer _centerX;
    private final Integer _centerY;

    MySettlement(String name, Integer startX, Integer endX, Integer startY, Integer endY, Integer tokenX, Integer tokenY) {
        this._name = name;
        this._startX = startX;
        this._endX = endX;
        this._startY = startY;
        this._endY = endY;
        // coordinates of the village token
        this._tokenX = tokenX;
        this._tokenY = tokenY;
        // coordinates of the village center
        this._centerX = this._startX + ((this._endX - this._startX) / 2);
        this._centerY = this._startY + ((this._endY - this._startY) / 2);
    }

    @Override
    public String getName() {
        return this._name;
    }

    @Override
    public Integer getTokenX() {
        return this._tokenX;
    }

    @Override
    public Integer getTokenY() {
        return this._tokenY;
    }

    @Override
    public Integer getCenterX() {
        return this._centerX;
    }

    @Override
    public Integer getCenterY() {
        return this._centerY;
    }

}
