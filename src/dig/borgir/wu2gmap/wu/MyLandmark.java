package dig.borgir.wu2gmap.wu;

/**
 *
 * @author Diggu Borgir
 */
class MyLandmark implements Landmark {

    private final String _name;
    private final Integer _x;
    private final Integer _y;

    MyLandmark(String name, Integer x, Integer y) {
        this._name = name;
        this._x = x;
        this._y = y;
    }

    @Override
    public String getName() {
        return this._name;
    }

    @Override
    public Integer getX() {
        return this._x;
    }

    @Override
    public Integer getY() {
        return this._y;
    }

}
