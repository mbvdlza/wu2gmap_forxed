package dig.borgir.wu2gmap.wu;

/**
 *
 * @author Diggu Borgir
 */
public interface House {
    
    public Integer getX();
    
    public Integer getY();
    
    public String getName();
    
}
