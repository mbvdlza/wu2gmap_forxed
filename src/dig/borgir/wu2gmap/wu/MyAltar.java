package dig.borgir.wu2gmap.wu;

/**
 *
 * @author Diggu Borgir
 */
class MyAltar implements Altar {

    private final String _name;
    private final String _desc;
    private final Integer _x;
    private final Integer _y;

    MyAltar(String name, String desc, Integer x, Integer y) {
        this._name = name;
        this._desc = desc;
        this._x = x;
        this._y = y;
    }

    @Override
    public String getName() {
        return this._name;
    }

    @Override
    public String getDescription() {
        return this._desc;
    }

    @Override
    public Integer getX() {
        return this._x;
    }

    @Override
    public Integer getY() {
        return this._y;
    }

}
