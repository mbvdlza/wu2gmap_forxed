package dig.borgir.wu2gmap.wu;

/**
 *
 * @author Diggu Borgir
 */
public interface StoneOfSoulfall {

    public String getName();

    public Integer getX();

    public Integer getY();

}
