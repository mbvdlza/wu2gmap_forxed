package dig.borgir.wu2gmap.wu;

import com.wurmonline.mesh.MeshIO;
import com.wurmonline.mesh.Tiles;
import dig.borgir.wu2gmap.config.Config;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Diggu Borgir
 */
class MyWuMap implements WuMap {

    private String _serverName;
    private String _mapName;
    private final Integer _width;
    private final Integer _height;
    private List<Settlement> _settlements;
    private List<Tower> _towers;
    private int[][] _buildTiles;
    private List<House> _houses;
    private Tiles.Tile[][] _undergroundTiles;
    private int[][] _undergroundWater;
    private List<CaveEntrance> _entrances;
    private Color[][] _surfaceTiles;
    private int[][] _surfaceWater;
    private List<StoneOfSoulfall> _stones;
    private List<Altar> _altars;
    private List<Landmark> _landmarks;

    private final Config _config;
    private Connection dbLogin;
    private Connection dbZones;
    private Connection dbItems;

    private final MeshIO surfaceMesh;
    private final MeshIO rockMesh;
    private final MeshIO caveMesh;

    MyWuMap(Config config) throws ClassNotFoundException, SQLException, IOException {
        this._config = config;

        this.openAllDatabases();
        this.surfaceMesh = MeshIO.open(config.getPathToSurfaceMesh());
        this.rockMesh = MeshIO.open(config.getPathToRockMesh());
        this.caveMesh = MeshIO.open(config.getPathToCaveMesh());

        this._width = this.surfaceMesh.getSize();
        this._height = this.surfaceMesh.getSize();

        this.readDbLoginServers();
        this.readDbSettlements();
        this.readDbTowers();
        this.readDbBuildTiles();
        this.readDbHouses();
        this.readDbStonesOfSoulfall();
        this.readDbAltars();

        this.readLandmarks();

        this.readSurface();
        this.readUnderground();

        this.closeAllDatabases();
    }

    @Override
    public String getServerName() {
        return this._serverName;
    }

    @Override
    public String getMapName() {
        return this._mapName;
    }

    @Override
    public Integer getMapWidth() {
        return this._width;
    }

    @Override
    public Integer getMapHeight() {
        return this._height;
    }

    @Override
    public List<Settlement> getSettlements() {
        return this._settlements;
    }

    @Override
    public List<Tower> getTowers() {
        return this._towers;
    }

    @Override
    public int[][] getBuildTiles() {
        return this._buildTiles;
    }

    @Override
    public List<House> getHouses() {
        return this._houses;
    }

    @Override
    public Tiles.Tile[][] getUndergroundTiles() {
        return this._undergroundTiles;
    }

    @Override
    public int[][] getUndergroundWater() {
        return this._undergroundWater;
    }

    @Override
    public List<CaveEntrance> getEntrances() {
        return this._entrances;
    }

    @Override
    public Color[][] getSurfaceTiles() {
        return this._surfaceTiles;
    }

    @Override
    public int[][] getSurfaceWater() {
        return this._surfaceWater;
    }

    @Override
    public List<StoneOfSoulfall> getStonesOfSoulfall() {
        return this._stones;
    }

    @Override
    public List<Altar> getAltars() {
        return this._altars;
    }

    @Override
    public List<Landmark> getLandmarks() {
        return this._landmarks;
    }

    private void openAllDatabases() throws SQLException, ClassNotFoundException {
        Class.forName("org.sqlite.JDBC");
        System.out.println("Opening " + this._config.getPathToDbLogin());
        this.dbLogin = DriverManager.getConnection("jdbc:sqlite:" + this._config.getPathToDbLogin());
        System.out.println("Opening " + this._config.getPathToDbZones());
        this.dbZones = DriverManager.getConnection("jdbc:sqlite:" + this._config.getPathToDbZones());
        System.out.println("Opening " + this._config.getPathToDbItems());
        this.dbItems = DriverManager.getConnection("jdbc:sqlite:" + this._config.getPathToDbItems());
    }

    private void closeAllDatabases() throws SQLException {
        System.out.println("Closing " + this._config.getPathToDbLogin());
        this.dbLogin.close();
        System.out.println("Closing " + this._config.getPathToDbZones());
        this.dbZones.close();
        System.out.println("Closing " + this._config.getPathToDbZones());
        this.dbItems.close();
    }

    private void readDbLoginServers() throws SQLException {
        String sql = "SELECT name, mapname FROM servers";
        PreparedStatement ps = this.dbLogin.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            this._serverName = rs.getString("name");
            this._mapName = rs.getString("name");
            //TODO if there's more rows in the SERVERS table, let's process only the first one
            break;
        }
        rs.close();
        ps.close();
    }

    private void readDbSettlements() throws SQLException {
        this._settlements = new ArrayList<>();
        String sql = "SELECT name, startx, endx, starty, endy, token FROM villages WHERE disbanded = 0";
        PreparedStatement ps = this.dbZones.prepareStatement(sql);
        String sql2 = "SELECT posx, posy FROM items WHERE wurmid = ?";
        PreparedStatement ps2 = this.dbItems.prepareStatement(sql2);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            String name = rs.getString("name");
            Integer startX = rs.getInt("startx");
            Integer endX = rs.getInt("endx");
            Integer startY = rs.getInt("starty");
            Integer endY = rs.getInt("endy");
            Long token = rs.getLong("token");
            Integer tokenX = null;
            Integer tokenY = null;
            ps2.setLong(1, token);
            ResultSet rs2 = ps2.executeQuery();
            while (rs2.next()) {
                tokenX = this.getTilePosition(rs2.getFloat("posx"));
                tokenY = this.getTilePosition(rs2.getFloat("posy"));
            }
            rs2.close();
            Settlement settlement = new MySettlement(name, startX, endX, startY, endY, tokenX, tokenY);
            this._settlements.add(settlement);
        }
        ps2.close();
        rs.close();
        ps.close();
    }

    private void readDbTowers() throws SQLException {
        this._towers = new ArrayList<>();
        String sql = "SELECT description, posx, posy FROM items WHERE name = 'guard tower'";
        PreparedStatement ps = this.dbItems.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            String desc = rs.getString("description");
            float posx = rs.getFloat("posx");
            float posy = rs.getFloat("posy");
            Integer x = this.getTilePosition(posx);
            Integer y = this.getTilePosition(posy);
            Tower tower = new MyTower(desc, x, y);
            this._towers.add(tower);
        }
        rs.close();
        ps.close();
    }

    private void readDbStonesOfSoulfall() throws SQLException {
        this._stones = new ArrayList<>();
        String sql = "SELECT description, posx, posy FROM items WHERE name = 'Stone of Soulfall'";
        PreparedStatement ps = this.dbItems.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            String desc = rs.getString("description");
            float posx = rs.getFloat("posx");
            float posy = rs.getFloat("posy");
            Integer x = this.getTilePosition(posx);
            Integer y = this.getTilePosition(posy);
            StoneOfSoulfall stone = new MyStoneOfSoulfall(desc, x, y);
            this._stones.add(stone);
        }
        rs.close();
        ps.close();
    }

    private void readDbAltars() throws SQLException {
        this._altars = new ArrayList<>();
        String sql = "SELECT name, description, posx, posy FROM items WHERE name LIKE '%altar%'";
        PreparedStatement ps = this.dbItems.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            String name = rs.getString("name");
            String desc = rs.getString("description");
            float posx = rs.getFloat("posx");
            float posy = rs.getFloat("posy");
            Integer x = this.getTilePosition(posx);
            Integer y = this.getTilePosition(posy);
            Altar altar = new MyAltar(name, desc, x, y);
            this._altars.add(altar);
        }
        rs.close();
        ps.close();
    }

    private void readDbBuildTiles() throws SQLException {
        this._buildTiles = new int[this._width][this._height];
        // TODO how does layer work exactly?
        String sql = "SELECT tilex, tiley FROM buildtiles WHERE layer = 0";
        PreparedStatement ps = this.dbZones.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Integer x = rs.getInt("tilex");
            Integer y = rs.getInt("tiley");
            this._buildTiles[x][y] = 1;
        }
        rs.close();
        ps.close();
    }

    private void readDbHouses() throws SQLException {
        this._houses = new ArrayList<>();
        //TODO only finished or all?
        String sql = "SELECT centerx, centery, name FROM structures";
        PreparedStatement ps = this.dbZones.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            Integer x = rs.getInt("centerx");
            Integer y = rs.getInt("centery");
            String name = rs.getString("name");
            this._houses.add(new MyHouse(x, y, name));
        }
        rs.close();
        ps.close();
    }

    private void readUnderground() {
        final MeshIO terrainMesh = caveMesh;
        final MeshIO heightMesh = rockMesh;

        this._undergroundTiles = new Tiles.Tile[this._width][this._height];
        this._undergroundWater = new int[this._width][this._height];
        this._entrances = new ArrayList<>();

        int lWidth = 16384;
        if (lWidth > this.rockMesh.getSize()) {
            lWidth = this.rockMesh.getSize();
        }
        int yo = this.rockMesh.getSize() - lWidth;
        if (yo < 0) {
            yo = 0;
        }
        int xo = this.rockMesh.getSize() - lWidth;
        if (xo < 0) {
            xo = 0;
        }

        // TODO WTF is this?
        final Random random = new Random();
        if (xo > 0) {
            xo = random.nextInt(xo);
        }
        if (yo > 0) {
            yo = random.nextInt(yo);
        }

        for (int x = 0; x < lWidth; x++) {
            for (int y = lWidth - 1; y >= 0; y--) {
                final short height = Tiles.decodeHeight(heightMesh.getTile(x + xo, y + yo));
                final byte tex = Tiles.decodeType(terrainMesh.getTile(x + xo, y + yo));
                Tiles.Tile tile = Tiles.getTile(tex);

                if (tile == null) {
                    tile = Tiles.Tile.TILE_CAVE;
                } else if (tile == Tiles.Tile.TILE_CAVE_EXIT) {
                    this._entrances.add(new MyCaveEntrance(x, y));
                }

                this._undergroundTiles[x][y] = tile;

                if (height < 0) {
                    // this tile is under water
                    this._undergroundWater[x][y] = 1;
                } else {
                    this._undergroundWater[x][y] = 0;
                }

            }
        }
    }

    private void readSurface() {
        final MeshIO terrainMesh = this.surfaceMesh;
        final MeshIO heightMesh = this.surfaceMesh;

        this._surfaceTiles = new Color[this._width][this._height];
        this._surfaceWater = new int[this._width][this._height];

        int lWidth = 16384;
        if (lWidth > this.surfaceMesh.getSize()) {
            lWidth = this.surfaceMesh.getSize();
        }
        int yo = this.surfaceMesh.getSize() - lWidth;
        if (yo < 0) {
            yo = 0;
        }
        int xo = this.surfaceMesh.getSize() - lWidth;
        if (xo < 0) {
            xo = 0;
        }

        // TODO WTF is this?
        final Random random = new Random();
        if (xo > 0) {
            xo = random.nextInt(xo);
        }
        if (yo > 0) {
            yo = random.nextInt(yo);
        }

        for (int x = 0; x < lWidth; x++) {
            for (int y = lWidth - 1; y >= 0; y--) {
                final short height = Tiles.decodeHeight(heightMesh.getTile(x + xo, y + yo));
                final byte tex = Tiles.decodeType(terrainMesh.getTile(x + xo, y + yo));
                final Tiles.Tile tile = Tiles.getTile(tex);

                final Color color;
                if (tile != null) {
                    color = tile.getColor();
                } else {
                    color = Tiles.Tile.TILE_DIRT.getColor();
                }
                this._surfaceTiles[x][y] = color;

                if (height < 0) {
                    // this tile is under water
                    this._surfaceWater[x][y] = 1;
                } else {
                    this._surfaceWater[x][y] = 0;
                }
            }
        }
    }

    /**
     * Converts absolute position to tile position (code snippet from WurmAPI
     * Item.class)
     *
     * @param pos
     * @return
     */
    private int getTilePosition(float pos) {
        // what sorcery is this?
        return (int) pos >> 2;
    }

    private void readLandmarks() throws FileNotFoundException, IOException {
        this._landmarks = new ArrayList<>();
        File file = new File("landmarks.txt");
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        while ((line = br.readLine()) != null) {
            line = line.trim();
            if (line.length() > 0) {
                String[] elements = StringUtils.split(line);
                Integer x = Integer.parseInt(elements[0]);
                Integer y = Integer.parseInt(elements[1]);
                String name = elements[2];
                for (int i = 3; i < elements.length; i++) {
                    name = name + " " + elements[i];
                }
                System.out.println(name);
                this._landmarks.add(new MyLandmark(name, x, y));
            }
        }
        br.close();
        fr.close();
    }

}
