package dig.borgir.wu2gmap.wu;

/**
 *
 * @author Diggu Borgir
 */
class MyCaveEntrance implements CaveEntrance {

    private final String _name;
    private final Integer _x;
    private final Integer _y;

    MyCaveEntrance(Integer x, Integer y) {
        //TODO add support for custom names
        this._name = "Entrance";
        this._x = x;
        this._y = y;
    }

    @Override
    public Integer getX() {
        return this._x;
    }

    @Override
    public Integer getY() {
        return this._y;
    }

    @Override
    public String getName() {
        return this._name;
    }
    
}
