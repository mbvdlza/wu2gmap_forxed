package dig.borgir.wu2gmap.wu;

import com.wurmonline.mesh.Tiles;
import java.awt.Color;
import java.util.List;

/**
 *
 * @author Diggu Borgir
 */
public interface WuMap {

    public String getServerName();

    public String getMapName();

    /**
     *
     * @return map size X (in tiles)
     */
    public Integer getMapWidth();

    /**
     *
     * @return map size Y (in tiles)
     */
    public Integer getMapHeight();

    public List<Settlement> getSettlements();

    public List<Tower> getTowers();

    public int[][] getBuildTiles();

    public List<House> getHouses();

    public Tiles.Tile[][] getUndergroundTiles();

    public int[][] getUndergroundWater();

    public List<CaveEntrance> getEntrances();

    public Color[][] getSurfaceTiles();

    public int[][] getSurfaceWater();

    public List<StoneOfSoulfall> getStonesOfSoulfall();

    public List<Altar> getAltars();

    public List<Landmark> getLandmarks();

}
