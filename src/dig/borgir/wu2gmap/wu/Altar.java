package dig.borgir.wu2gmap.wu;

/**
 *
 * @author Diggu Borgir
 */
public interface Altar {

    public String getName();

    public String getDescription();

    public Integer getX();

    public Integer getY();

}
