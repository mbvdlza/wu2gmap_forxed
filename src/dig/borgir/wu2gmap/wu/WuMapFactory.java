package dig.borgir.wu2gmap.wu;

import dig.borgir.wu2gmap.config.Config;
import java.io.IOException;
import java.sql.SQLException;

/**
 *
 * @author Diggu Borgir
 */
public class WuMapFactory {

    public static WuMap createWuMap(Config config) throws ClassNotFoundException, SQLException, IOException {
        return new MyWuMap(config);
    }

}
