package dig.borgir.wu2gmap.wu;

/**
 *
 * @author Diggu Borgir
 */
public interface Settlement {
    
    public String getName();
    
    public Integer getTokenX();
    
    public Integer getTokenY();
    
    public Integer getCenterX();
    
    public Integer getCenterY();
    
}
