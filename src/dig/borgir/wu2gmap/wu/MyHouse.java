package dig.borgir.wu2gmap.wu;

/**
 *
 * @author Diggu Borgir
 */
class MyHouse implements House {

    private final Integer _x;
    private final Integer _y;
    private final String _name;

    MyHouse(Integer x, Integer y, String name) {
        this._x = x;
        this._y = y;
        this._name = name;
    }

    @Override
    public Integer getX() {
        return this._x;
    }

    @Override
    public Integer getY() {
        return this._y;
    }

    @Override
    public String getName() {
        return this._name;
    }

}
