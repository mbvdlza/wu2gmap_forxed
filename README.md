# wu2gmap
Google Maps generator for Wurm Unlimited


User Guide: 

https://github.com/diggu/wu2gmap/wiki/Guide


Download

https://github.com/diggu/wu2gmap/releases/



Examples

https://github.com/diggu/wu2gmap/wiki/Examples



The official forum thread: 

http://forum.wurmonline.com/index.php?/topic/134415-wu2gmap-google-maps-generator-for-wu/
